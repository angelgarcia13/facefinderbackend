﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using FaceFinder.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FaceFinder.BackOffice.Helpers
{
    public abstract class BaseView<TModel> : System.Web.Mvc.WebViewPage<TModel> where TModel : class
    {
        public ApplicationUser CurrentUser { get; private set; }
        protected override void InitializePage()
        {
            //Context = System.Web.HttpContext.Current;
            CurrentUser = Context.GetOwinContext()
                .GetUserManager<ApplicationUserManager>()
                 .FindById(Context.User.Identity.GetUserId());
            base.InitializePage();
        }

        public bool IsAdmin
        {
            get { return Context.User.IsInRole("Administrators"); }
        }

        public bool IsAgent
        {
            get { return Context.User.IsInRole("Agents"); }
        }
    }
}