﻿var PersonViewModule = (function () {

    var _init = function (model) {
        // private
        $(".personDeleteButton").click(function (e) {
            var id = $(this).data("id");
            var r = confirm("Estás seguro que desear eliminar este elemento?");
            if (r == true) {
                $.post("/People/Delete/" + id, function () {
                    $("#pageSpinner").fadeIn('slow');
                    location.reload();
                }).fail(function () {
                    alert("Ha ocurrido un error");
                });
            }
        });
    };

    var init = function (model) {
        // public
        $("#pageSpinner").fadeIn('slow');
        $(document).ready(function () {
            _init(model);
            $("#pageSpinner").fadeOut('slow');
        });
    };

    return {
        init: init
    };

})();