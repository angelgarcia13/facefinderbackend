﻿var FacesViewModule = (function () {

    var _init = function (model) {
        // private
        $(".faceDeleteButton").click(function (e) {
            var id = $(this).data("id");
            var r = confirm("Estás seguro que desear eliminar este elemento?");
            if (r == true) {
                $.post("/People/DeleteFace/" + id, function () {
                    $("#pageSpinner").fadeIn('slow');
                    $(e.target.closest(".snip1174")).parent().remove();
                    $("#pageSpinner").fadeOut('slow');
                }).fail(function () {
                    alert("Ha ocurrido un error");
                });
            }

        });
    };

    var init = function (model) {
        // public
        $("#pageSpinner").fadeIn('slow');
        $(document).ready(function () {
            _init(model);
            $("#pageSpinner").fadeOut('slow');
        });
    };

    return {
        init: init
    };

})();