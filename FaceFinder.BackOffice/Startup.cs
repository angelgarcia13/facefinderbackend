﻿using FaceFinder.Data.Helpers;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FaceFinder.BackOffice.Startup))]
namespace FaceFinder.BackOffice
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            StorageHelper.InitContainers();
        }
    }
}
