﻿using FaceFinder.Data.DBContext;
using FaceFinder.Models.Api.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FaceFinder.BackOffice.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            var alertResults = db.Alerts
                .Include(a => a.Person)
                .Include(a => a.UserProfile)
                .ToList();
            var numberOfAccounts = db.UserProfiles.Count();
            var numberOfProfilesWanted = db.Persons.Count();

            var results = new DashboardModel {
                Alerts = alertResults,
                NumberOfAccounts = numberOfAccounts,
                NumberOfProfilesWanted = numberOfProfilesWanted
            };
            return View(results);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}