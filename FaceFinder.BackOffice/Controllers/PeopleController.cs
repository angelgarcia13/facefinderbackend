﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FaceFinder.Data.DBContext;
using FaceFinder.Models.Entities;
using FaceFinder.Models.Api.Models;
using AutoMapper;
using FaceFinder.Data.Helpers;
using FaceFinder.BackOffice.Models;
using System.Threading.Tasks;
using Microsoft.ProjectOxford.Face;

namespace FaceFinder.BackOffice.Controllers
{
    public class PeopleController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: People
        public ActionResult Index()
        {
            var persons = db.Persons.Include(p => p.PersonGroup);
            return View(persons.ToList());
        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: People/Create
        public ActionResult Create()
        {
            //ViewBag.PersonGroupId = new SelectList(db.PersonGroups, "Id", "GroupGuid");
            return View();
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> Create([Bind(Include = "Id,FirstName,LastName,NickName,WantedReasons,Notes,Birthdate,Gender")] PersonVM model)
        {
            if (ModelState.IsValid)
            {
                string personGroupId = null;
                var config = new MapperConfiguration(cfg => cfg.CreateMap<PersonVM, Person>());
                var mapper = config.CreateMapper();
                var newPerson = mapper.Map<PersonVM, Person>(model);
                newPerson.DateRegistered = DateTime.UtcNow;
                db.Persons.Add(newPerson);
                db.SaveChanges();
                //return RedirectToAction("AddFaces", new { id = newPerson.Id });

                try
                {
                    var person = await FaceRecognitionHelper.CreatePerson(model.FirstName, newPerson.Id);
                    if (!person.Success)
                    {
                        ViewBag.Message = new MessageViewModel
                        {
                            IsError = true,
                            Message = "Favor intente más tarde, la base de datos de perfiles está siendo sincronizada"
                        };
                        db.Entry(newPerson).State = EntityState.Deleted;
                        db.SaveChanges();
                    }
                    else
                    {
                        newPerson.HasFacePersonId = true;
                        newPerson.FacePersonId = person.PersonId.ToString();
                        personGroupId = person.PersonGroupId.ToString();

                        newPerson.PersonGroupId = person.PersonGroupId;
                        db.Entry(newPerson).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("AddFaces", new { id = newPerson.Id });
                    }

                }
                catch (Exception e)
                {
                    //Roll back
                    db.Entry(newPerson).State = EntityState.Deleted;
                    db.SaveChanges();
                    //Delete Added Person
                    if (newPerson.PersonGroupId != null && newPerson.FacePersonId != null)
                    {
                        try
                        {
                            await FaceRecognitionHelper.DeletePerson(newPerson.FacePersonId, newPerson.PersonGroup.GroupGuid);

                        }
                        catch (Exception)
                        {
                            //TODO
                        }
                    }

                    ViewBag.Message = new MessageViewModel
                    {
                        IsError = true,
                        Message = "Favor intente más tarde, ha ocurrido un error tratando de crear el perfil"
                    };
                }

            }

            //ViewBag.PersonGroupId = new SelectList(db.PersonGroups, "Id", "GroupGuid", person.PersonGroupId);
            return View(model);
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Person, PersonVM>());
            var mapper = config.CreateMapper();
            var editPerson = mapper.Map<Person, PersonVM>(person);
            return View(editPerson);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,NickName,WantedReasons,Notes,Birthdate,Gender,Disabled")] PersonVM person)
        {
            if (ModelState.IsValid)
            {
                var personEdited = db.Persons.Find(person.Id);
                personEdited.FirstName = person.FirstName;
                personEdited.LastName = person.LastName;
                personEdited.NickName = person.NickName;
                personEdited.WantedReasons = person.WantedReasons;
                personEdited.Notes = person.Notes;
                personEdited.Birthdate = person.Birthdate;
                personEdited.Gender = person.Gender;
                personEdited.Disabled = person.Disabled;
                db.Entry(personEdited).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Person person = db.Persons.Include(p => p.FaceImages).FirstOrDefault(p => p.Id == id);
            if (person == null)
            {
                return HttpNotFound();
            }
            db.Persons.Remove(person);
            db.SaveChanges();
            foreach (var item in person.FaceImages)
            {
                await StorageHelper.DeleteImage(item.PersonId.ToString(), item.Id, item.Extension);
            }
            //TODO: Delete person in face api
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        // GET: People/Faces/5
        public ActionResult Faces(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Include(p => p.FaceImages).FirstOrDefault(p => p.Id == id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }
        // GET: People/AddFaces/5
        public ActionResult AddFaces(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/AddFaces/5
        [HttpPost, ActionName("AddFaces")]
        public async System.Threading.Tasks.Task<ActionResult> AddFacesUpload(Person model)
        {
            if (model == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Include(p => p.PersonGroup).FirstOrDefault(p => p.Id == model.Id);
            if (person == null)
            {
                return HttpNotFound();
            }
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];
                var ext = System.IO.Path.GetExtension(file.FileName);
                int MaxContentLength = 1024 * 1024 * 4; //Size = 4 MB

                FaceImage image = new FaceImage
                {
                    PersonId = person.Id,
                    Extension = ext
                };
                db.FaceImages.Add(image);
                db.SaveChanges();
                var byteArray = StorageHelper.ConverToBytes(file);
                //Check file size, if is more than 4 MB, reduce it until get it
                if (byteArray.Length > MaxContentLength)
                {
                    byteArray = StorageHelper.ResizeImageFileSize(byteArray, MaxContentLength);
                }

                await StorageHelper.UploadImage(model.Id.ToString(), image.Id, ext, byteArray);
                try
                {
                    var persistedFaceId = await FaceRecognitionHelper.AddPersonFace(new Guid(person.FacePersonId), person.Id.ToString(), person.PersonGroup.GroupGuid.ToString(), byteArray);
                    image.PersistedFaceId = persistedFaceId;
                    db.Entry(image).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (FaceAPIException)
                {
                    db.Entry(image).State = EntityState.Deleted;
                    db.SaveChanges();
                    await StorageHelper.DeleteImage(model.Id.ToString(), image.Id, ext);

                    ViewBag.Message = new MessageViewModel
                    {
                        IsError = true,
                        Message = "Favor intente más tarde, la base de datos de perfiles está siendo sincronizada"
                    };
                }

            }
            return RedirectToAction("Faces", new { id = person.Id });
        }
        // POST: People/DeleteFace/5
        [HttpPost, ActionName("DeleteFace")]
        public async Task<ActionResult> DeleteFace(int id)
        {
            FaceImage image = db.FaceImages
                .Include(i => i.Person.PersonGroup)
                .FirstOrDefault(i => i.Id == id);
            if (image == null)
            {
                return HttpNotFound();
            }
            //Delete person face in face api
            try
            {
                await FaceRecognitionHelper.DeletePersonFace(new Guid(image.PersistedFaceId), new Guid(image.Person.FacePersonId), image.Person.PersonGroup.GroupGuid);
            }
            catch (FaceAPIException ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
            db.FaceImages.Remove(image);
            db.SaveChanges();
            await StorageHelper.DeleteImage(image.PersonId.ToString(), image.Id, image.Extension);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
