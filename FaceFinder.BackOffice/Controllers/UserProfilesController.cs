﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FaceFinder.Data.DBContext;
using FaceFinder.Models.Entities;
using FaceFinder.Models.Api.Models;
using Microsoft.AspNet.Identity;
using FaceFinder.BackOffice.Models;
using Microsoft.AspNet.Identity.Owin;

namespace FaceFinder.BackOffice.Controllers
{
    public class UserProfilesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;
        public UserProfilesController() { }

        public UserProfilesController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: UserProfiles
        public async Task<ActionResult> Index()
        {
            var userProfiles = db.UserProfiles.Include(u => u.ApplicationUser);
            return View(await userProfiles.ToListAsync());
        }

        // GET: UserProfiles/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile userProfile = await db.UserProfiles.FindAsync(id);
            if (userProfile == null)
            {
                return HttpNotFound();
            }
            return View(userProfile);
        }

        // GET: UserProfiles/Create
        public ActionResult Create()
        {
            //ViewBag.ApplicationUserId = new SelectList(db.ApplicationUsers, "Id", "Email");
            return View();
        }

        // POST: UserProfiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ApplicationUserId,FirstName,LastName,Email,Password")] UserModel userProfile)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = userProfile.Email, Email = userProfile.Email };
                var result = await UserManager.CreateAsync(user, userProfile.Password);
                if (result.Succeeded)
                {
                    var userRegistered = db.Users.FirstOrDefault(u => u.UserName == userProfile.Email);
                    db.UserProfiles.Add(new UserProfile
                    {
                        ApplicationUserId = userRegistered.Id,
                        DateRegistered = DateTime.UtcNow,
                        Disabled = false,
                        FirstName = userProfile.FirstName,
                        LastName = userProfile.LastName
                    });
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Message = new MessageViewModel
                    {
                        IsError = true,
                        Message = "Error al tratar de crear el usuario"
                    };
                }
            }
            return View(userProfile);
        }

        // GET: UserProfiles/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile userProfile = await db.UserProfiles.Include(u => u.ApplicationUser).FirstOrDefaultAsync(u => u.ApplicationUserId == id);
            if (userProfile == null)
            {
                return HttpNotFound();
            }
            var resutl = new UserEditModel {
                ApplicationUserId = userProfile.ApplicationUserId,
                Disabled = userProfile.Disabled,
                Email = userProfile.ApplicationUser.Email,
                FirstName = userProfile.FirstName,
                LastName = userProfile.LastName
            };
            //ViewBag.ApplicationUserId = new SelectList(db.ApplicationUsers, "Id", "Email", userProfile.ApplicationUserId);
            return View(resutl);
        }

        // POST: UserProfiles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ApplicationUserId,FirstName,LastName,Email,Password,Disabled")] UserEditModel userProfile)
        {
            if (ModelState.IsValid)
            {
                var userRegistered = await db.UserProfiles.Include(p => p.ApplicationUser)
                    .FirstOrDefaultAsync(p => p.ApplicationUserId == userProfile.ApplicationUserId);
                userRegistered.ApplicationUser.Email = userProfile.Email;
                userRegistered.ApplicationUser.UserName = userProfile.Email;
                userRegistered.Disabled = userProfile.Disabled;
                userRegistered.FirstName = userProfile.FirstName;
                userRegistered.LastName = userProfile.LastName;
                if (!string.IsNullOrWhiteSpace(userProfile.Password))
                {
                    
                    await UserManager.RemovePasswordAsync(userRegistered.ApplicationUserId);
                    await UserManager.AddPasswordAsync(userRegistered.ApplicationUserId, userProfile.Password);
                }
                db.Entry(userRegistered).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            //ViewBag.ApplicationUserId = new SelectList(db.ApplicationUsers, "Id", "Email", userProfile.ApplicationUserId);
            return View(userProfile);
        }

        // GET: UserProfiles/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile userProfile = await db.UserProfiles.FindAsync(id);
            if (userProfile == null)
            {
                return HttpNotFound();
            }
            return View(userProfile);
        }

        // POST: UserProfiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            UserProfile userProfile = await db.UserProfiles.FindAsync(id);
            db.UserProfiles.Remove(userProfile);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
