﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models
{
    public static class ModelsUtil
    {
        public static int Age(DateTime birthdate)
        {
            DateTime now = DateTime.UtcNow;
            int age = now.Year - birthdate.Year;
            if (now < birthdate.AddYears(age)) age--;
            return age;
        }
    }
}
