﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models.Api.Models
{
    public class Profile
    {
        public string ApplicationUserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        //public DateTime? Birthdate { get; set; }
        //public string Gender { get; set; }
        public DateTime? DateRegistered { get; set; }
        public DateTime? DateLastLogin { get; set; }
    }
}
