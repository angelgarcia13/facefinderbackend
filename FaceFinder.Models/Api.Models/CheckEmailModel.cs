﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models.Api.Models
{
    public class CheckEmailModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

    }
}
