﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models.Api.Models
{
    public class AlertModel
    {
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public bool? AutoAlert { get; set; }
    }
}
