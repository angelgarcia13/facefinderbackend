﻿using FaceFinder.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models.Api.Models
{
    public class DashboardModel
    {
        public List<Alert> Alerts { get; set; }
        public int NumberOfAccounts { get; set; }
        public int NumberOfProfilesWanted { get; set; }

    }
}
