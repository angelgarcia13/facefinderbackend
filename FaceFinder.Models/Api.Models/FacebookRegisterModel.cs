﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models.Api.Models
{
    public class FacebookRegisterModel
    {
        [Required]
        [StringLength(128)]
        public string IdentityUserId { get; set; }
        [Display(Name = "Birthdate")]
        public string Birthdate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
    }
}
