﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models.Api.Models
{
    public class CheckuserModel
    {
        [Required]
        public string Username { get; set; }
    }
}
