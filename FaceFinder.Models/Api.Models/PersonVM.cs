﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models.Api.Models
{
    public class PersonVM
    {
        public int Id { get; set; }
        [DisplayName("Nombre")]
        [Required]
        public string FirstName { get; set; }
        [DisplayName("Apellido")]
        [Required]
        public string LastName { get; set; }
        [DisplayName("Apodo")]
        [Required]
        public string NickName { get; set; }
        [DisplayName("Razones de búsqueda")]
        [Required]
        public string WantedReasons { get; set; }
        [DisplayName("Notas")]
        [Required]
        public string Notes { get; set; }
        [DisplayName("Fecha de Nacimiento")]
        public DateTime? Birthdate { get; set; }
        [DisplayName("Sexo")]
        [Required]
        public string Gender { get; set; }
        [DisplayName("Estatus")]
        public bool Disabled { get; set; }
    }
}
