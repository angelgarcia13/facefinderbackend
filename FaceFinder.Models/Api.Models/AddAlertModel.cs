﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models.Api.Models
{
    public class AddAlertModel
    {
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public int PersonId { get; set; }
    }
}
