﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models.Api.Models
{
    public class LoginResponse
    {
        /*
          "access_token":"imSXTs2OqSrGWzsFQhIXziFCO3rF...",
          "token_type":"bearer",
          "expires_in":1209599,
          "userName":"alice@example.com",
         */
        public string access_token { get; set; }
        public string token_type { get; set; }
        public long expires_in { get; set; }
        public string userName { get; set; }
        public string names { get; set; }
        public string email { get; set; }
        public bool hasPassword { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        //public string birthday { get; set; }
        //public string gender { get; set; }
        public string message { get; set; }
    }
}
