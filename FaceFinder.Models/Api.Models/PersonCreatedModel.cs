﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models.Api.Models
{
    public class PersonCreatedModel
    {
        public Guid PersonId { get; set; }
        public bool Success { get; set; }
        public int PersonGroupId { get; set; }
    }
}
