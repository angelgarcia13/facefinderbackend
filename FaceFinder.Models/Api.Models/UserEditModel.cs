﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models.Api.Models
{
    public class UserEditModel
    {
        [Required]
        public string ApplicationUserId { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [DisplayName("Nombre")]
        public string FirstName { get; set; }
        [Required]
        [DisplayName("Apellido")]
        public string LastName { get; set; }
        [Required]
        public bool Disabled { get; set; }
    }
}
