﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models.Entities
{
    public class Person
    {
        public int Id { get; set; }
        [DisplayName("Nombre")]
        public string FirstName { get; set; }
        [DisplayName("Apellido")]
        public string LastName { get; set; }
        [DisplayName("Apodo")]
        public string NickName { get; set; }
        [DisplayName("Razones de búsqueda")]
        public string WantedReasons { get; set; }
        [DisplayName("Notas")]
        public string Notes { get; set; }
        [DisplayName("Fecha de Nacimiento")]
        public DateTime? Birthdate { get; set; }
        [DisplayName("Sexo")]
        public string Gender { get; set; }
        public DateTime? DateRegistered { get; set; }
        public DateTime? DateLastLogin { get; set; }
        [DisplayName("Estatus")]
        public bool Disabled { get; set; }
        public string FacePersonId { get; set; }
        public bool HasFacePersonId { get; set; }
        public int? PersonGroupId { get; set; }
        public virtual PersonGroup PersonGroup { get; set; }
        public virtual ICollection<FaceImage> FaceImages { get; set; }
        [DisplayName("Nombre")]
        public string Name
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
        }
        public string Image
        {
            get
            {
                if (FaceImages != null && FaceImages.Count > 0)
                {
                    return FaceImages.First().ImageUrl;
                }
                else
                {
                    return null;
                }
            }
        }
        public bool HasImage
        {
            get
            {
                if (Image != null)
                {
                    return true;
                }
                return false;
            }
        }
        public bool HasNoImage
        {
            get
            {
                return !HasImage;
            }
        }
        public string Age
        {
            get
            {
                if (Birthdate.HasValue)
                {
                    return $"{ModelsUtil.Age(Birthdate.Value)} años";
                }
                return null;
            }
        }
        public bool HasAge
        {
            get
            {
                if (Birthdate.HasValue)
                {
                    return true;
                }
                return false;
            }
        }
        public string FormattedNickName
        {
            get
            {
                if (NickName != null)
                {
                    return $"({NickName})";
                }
                return null;
            }
        }

    }
}
