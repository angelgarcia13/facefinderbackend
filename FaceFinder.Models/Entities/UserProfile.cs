﻿using FaceFinder.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models.Entities
{
    public partial class UserProfile
    {
        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        //public DateTime? Birthdate { get; set; }

        //public string Gender { get; set; }

        public DateTime? DateRegistered { get; set; }

        public DateTime? DateLastLogin { get; set; }
        public bool Disabled { get; set; }

        [DisplayName("Nombre")]
        public string Name
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
        }

    }
}
