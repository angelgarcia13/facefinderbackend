﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models.Entities
{
    public class Alert
    {
        [DisplayName("Id")]
        public int Id { get; set; }
        [DisplayName("Fecha")]
        public DateTime DateCreated { get; set; }
        [DisplayName("Longitud")]
        public double? Longitude { get; set; }
        [DisplayName("Latitud")]
        public double? Latitude { get; set; }
        public string UserProfileApplicationUserId { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        public int PersonId { get; set; }
        public virtual Person Person { get; set; }

    }
}
