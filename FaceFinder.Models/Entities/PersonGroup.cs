﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models.Entities
{
    public class PersonGroup
    {
        public int Id { get; set; }
        public string GroupGuid { get; set; }
        public bool Completed { get; set; }
    }
}
