﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Models.Entities
{
    public class FaceImage
    {
        public int Id { get; set; }
        public string Extension { get; set; }
        public string ContainerUrl { get; set; }
        public string PersistedFaceId { get; set; }
        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
        public string ImageUrl
        {
            get { return GetUrl(); }
        }
        private string GetUrl()
        {
            return ConfigurationManager.AppSettings["BlobStorageHost"].ToString() + "faces/" + this.PersonId + "/" + this.Id + "-small"+ this.Extension;
        }
    }
}
