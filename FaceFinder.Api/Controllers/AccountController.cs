﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using FaceFinder.Api.Models;
using FaceFinder.Api.Providers;
using FaceFinder.Api.Results;
using FaceFinder.Models.Entities;
using FaceFinder.Models.Api.Models;
using FaceFinder.Api.CustomCode;
using FaceFinder.Data.DBContext;
using System.Linq;
using System.Net.Http.Headers;
using System.Data.Entity;

namespace FaceFinder.Api.Controllers
{
    [CustomAuthorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            using (var dbContext = new ApplicationDbContext())
            {
                var tokenFound = dbContext.Tokens.FirstOrDefault(x => x.Code == Request.Headers.Authorization.Parameter);
                if (tokenFound != null)
                {
                    dbContext.Entry(tokenFound).State = System.Data.Entity.EntityState.Deleted;
                    dbContext.SaveChanges();
                }
                else
                {
                    return BadRequest("Invalid Token");
                }
            }
            return Ok();
        }

        // POST api/Account/LogoutAll
        [Route("LogoutAll")]
        public IHttpActionResult LogoutAll()
        {
            try
            {
                Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);

                var userLogged = User.Identity.GetUserName();
                using (var dbContext = new ApplicationDbContext())
                {
                    dbContext.Tokens.RemoveRange(dbContext.Tokens.Where(x => x.ApplicationUser.UserName.Equals(userLogged, StringComparison.InvariantCultureIgnoreCase)));
                    dbContext.SaveChanges();
                }
            }
            catch (Exception)
            {

                return BadRequest("Error deleting the tokens");
            }

            return Ok();
        }

        // POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userId = UserManager.FindByName(User.Identity.GetUserName()).Id;
            IdentityResult result = await UserManager.ChangePasswordAsync(userId, model.OldPassword,
                model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/SetPassword
        [Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userId = UserManager.FindByName(User.Identity.GetUserName()).Id;
            IdentityResult result = await UserManager.AddPasswordAsync(userId, model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/AddExternalLogin
        [Route("AddExternalLogin")]
        public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = UserManager.FindByName(User.Identity.GetUserName());
            
            //Try to get the info from the provider
            ExternalLoginData externalData = await GetExternalUserInfo(model);

            if (externalData == null)
            {
                return BadRequest("Couldn't get the information from " + model.LoginProvider);
            }
            
            //Verify that the external login is not being used already

            if (user.Logins.Any(l => l.LoginProvider.Equals(model.LoginProvider, StringComparison.CurrentCultureIgnoreCase) && l.ProviderKey.Equals(externalData.ProviderKey)))
            {
                return BadRequest("The external login is already associated with this account.");
            }


            IdentityResult result = await UserManager.AddLoginAsync(user.Id,
                new UserLoginInfo(model.LoginProvider, externalData.ProviderKey));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }
        
        private async Task<ExternalLoginData> GetExternalUserInfo(AddExternalLoginBindingModel model)
        {
            if (model.LoginProvider.Equals("Facebook", StringComparison.CurrentCultureIgnoreCase))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://graph.facebook.com/v2.6/me");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("me?access_token=" + model.ExternalAccessToken);
                    if (response.IsSuccessStatusCode)
                    {
                        FacebookResponse result = await response.Content.ReadAsAsync<FacebookResponse>();
                        return new ExternalLoginData
                        {
                            LoginProvider = model.LoginProvider,
                            ProviderKey = result.id
                        };
                    }
                    else
                    {
                        return null;
                    }
                }
            }else if (model.LoginProvider.Equals("Google", StringComparison.CurrentCultureIgnoreCase))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://www.googleapis.com/userinfo/v2/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", model.ExternalAccessToken);
                    HttpResponseMessage response = await client.GetAsync("me");
                    if (response.IsSuccessStatusCode)
                    {
                        GoogleResponse result = await response.Content.ReadAsAsync<GoogleResponse>();
                        return new ExternalLoginData
                        {
                            LoginProvider = model.LoginProvider,
                            ProviderKey = result.id
                        };
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return null;

        }
        

        // POST api/Account/RemoveLogin
        [Route("RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            var user = UserManager.FindByName(User.Identity.GetUserName());

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await UserManager.RemovePasswordAsync(user.Id);
            }
            else
            {
                result = await UserManager.RemoveLoginAsync(user.Id,
                    new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogins
        [Route("ExternalLogins")]
        public IEnumerable<UserLoginInfoViewModel> GetExternalLogins()
        {
            List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();
            var user = UserManager.FindByName(User.Identity.GetUserName());
            foreach (var login in user.Logins)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = login.LoginProvider,
                    ProviderKey = login.ProviderKey
                });
            }
            return logins;
        }
        // POST api/Account/Register
        /*
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            UserProfile appUser;
            LoginResponse tokenIssued;
            try
            {
                appUser = RegisterInAppUserTable(model, user);


            }
            catch (Exception)
            {
                // Roll back
                UserManager.Delete(user);
                throw;
            }
            var message = "Account created successfully";
            if (model.ExternalLogin != null)
            {
                try
                {
                    //Try add external login
                    if (model.ExternalLogin.LoginProvider != null && model.ExternalLogin.ProviderKey != null)
                    {
                        if (model.ExternalLogin.LoginProvider.Equals("Facebook", StringComparison.InvariantCultureIgnoreCase))
                        {
                            //Add the login to the new user we have created
                            //Verify that the external login is not being used already

                            if (user.Logins.Any(l => l.LoginProvider.Equals(model.ExternalLogin.LoginProvider, StringComparison.CurrentCultureIgnoreCase) && l.ProviderKey.Equals(model.ExternalLogin.ProviderKey)))
                            {
                                message = "Account created, could not associate social login: The external login is already associated with this account";
                            }
                            else
                            {
                                IdentityResult operationResults = await UserManager.AddLoginAsync(user.Id,
                                new UserLoginInfo(model.ExternalLogin.LoginProvider, model.ExternalLogin.ProviderKey));

                                if (!operationResults.Succeeded)
                                {
                                    var errors = GetErrorResult(operationResults);
                                    message = "Account created, could not associate social login: The external login is already associated with an account";
                                }
                            }
                        }
                        else if (model.ExternalLogin.LoginProvider.Equals("Google", StringComparison.InvariantCultureIgnoreCase))
                        {
                            //Add the login to the new user we have created
                            //Verify that the external login is not being used already

                            if (user.Logins.Any(l => l.LoginProvider.Equals(model.ExternalLogin.LoginProvider, StringComparison.CurrentCultureIgnoreCase) && l.ProviderKey.Equals(model.ExternalLogin.ProviderKey)))
                            {
                                message = "Account created, could not associate social login: The external login is already associated with this account";
                            }
                            else
                            {
                                IdentityResult operationResults = await UserManager.AddLoginAsync(user.Id,
                                new UserLoginInfo(model.ExternalLogin.LoginProvider, model.ExternalLogin.ProviderKey));

                                if (!operationResults.Succeeded)
                                {
                                    var errors = GetErrorResult(operationResults);
                                    message = "Account created, could not associate social login: The external login is already associated with an account";
                                }
                            }
                        }
                        else
                        {
                            message = "Account created, could not associate social login: Not Supported Login Provider";
                        }
                    }
                }
                catch (Exception e)
                {

                    message = "Account created, could not associate social login";
                }
            }

            try
            {
                //Login the new user
                tokenIssued = await LoginRegisteredUser(new LoginModel
                {
                    UserName = model.Email,
                    Password = model.Password,
                    grant_type = "password"
                });
                tokenIssued.email = model.Email;
                tokenIssued.names = model.FirstName + " " + model.LastName;
                tokenIssued.hasPassword = true;
                tokenIssued.message = message;
            }
            catch (Exception)
            {
                // This should never happen...
                throw;
            }
            //Send Confirm Account Email...
            try
            {
                var confirmEmail = await ResendEmail(new CheckEmailModel
                {
                    Email = model.Email
                });
            }
            catch (Exception mailEx)
            {

                //throw;
            }
            return Ok(tokenIssued);
        }
        */
        private UserProfile RegisterInAppUserTable(RegisterBindingModel model, ApplicationUser user)
        {
            var AppUser = new UserProfile()
            {
                ApplicationUserId = user.Id,
                //Birthdate = model.Birthdate,
                FirstName = model.FirstName,
                LastName = model.LastName,
                //Gender = model.Gender,
                DateRegistered = DateTime.Now
            };

            using (var context = new ApplicationDbContext())
            {
                context.UserProfiles.Add(AppUser);
                context.SaveChanges();
            }


            //Return token instead of User

            return AppUser;
        }

        private async Task<LoginResponse> LoginRegisteredUser(LoginModel loginModel)
        {
            using (var client = new HttpClient())
            {
                // Getting base URL
                var url = Request.RequestUri.GetLeftPart(UriPartial.Authority);
                client.BaseAddress = new Uri(url + "/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // HTTP POST
                try
                {
                    var content = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("UserName", loginModel.UserName),
                        new KeyValuePair<string, string>("Password", loginModel.Password),
                        new KeyValuePair<string, string>("grant_type", loginModel.grant_type)
                    });

                    HttpResponseMessage response = await client.PostAsync("token", content);
                    response.EnsureSuccessStatusCode();    // Throw if not a success code.
                    var tokenResponse = await response.Content.ReadAsAsync<LoginResponse>();
                    return tokenResponse;
                }
                catch (HttpRequestException e)
                {
                    // Handle exception.
                    throw e;
                }

            }
        }

        [AllowAnonymous]
        [Route("FacebookLogin")]
        public async Task<IHttpActionResult> FacebookLogin(FBLoginBindingModel Request)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://graph.facebook.com/v2.6/me");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("me?fields=email%2Cfirst_name%2Clast_name%2Cgender&access_token=" + Request.Token);
                if (response.IsSuccessStatusCode)
                {
                    FacebookResponse result = await response.Content.ReadAsAsync<FacebookResponse>();
                    LoginResponse tokenIssued;
                    ApplicationUser user;
                    bool HasPassword = true;
                    try
                    {
                        //Check if the login exists
                        
                        using (var db = new ApplicationDbContext())
                        {
                            user = db.Users.Include("UserProfile").FirstOrDefault(a => a.Logins.Any(l => l.LoginProvider.Equals("Facebook", StringComparison.CurrentCultureIgnoreCase) && l.ProviderKey.Equals(result.id, StringComparison.CurrentCultureIgnoreCase)));
                        }

                        if (user == null)
                        {
                            return NotFound();//This Facebook account is not associated to any user
                        }
                       
                        tokenIssued = loginUserFromExternal(user, user.UserProfile.FirstName + " " + user.UserProfile.LastName);
                        tokenIssued.hasPassword = HasPassword;
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                    return Ok(tokenIssued);
                }
                else
                {
                    return BadRequest("Couldn't get FB information");
                }
            }

        }

        [AllowAnonymous]
        [Route("GoogleLogin")]
        public async Task<IHttpActionResult> GoogleLogin(GoogleLoginBindingModel Request)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://www.googleapis.com/userinfo/v2/me");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Request.Token);
                HttpResponseMessage response = await client.GetAsync("me");
                if (response.IsSuccessStatusCode)
                {
                    GoogleResponse result = await response.Content.ReadAsAsync<GoogleResponse>();
                    LoginResponse tokenIssued;
                    ApplicationUser user;
                    bool HasPassword = true;
                    try
                    {
                        //Check if the login exists

                        using (var db = new ApplicationDbContext())
                        {
                            user = db.Users.Include("UserProfile").FirstOrDefault(a => a.Logins.Any(l => l.LoginProvider.Equals("Google", StringComparison.CurrentCultureIgnoreCase) && l.ProviderKey.Equals(result.id, StringComparison.CurrentCultureIgnoreCase)));
                        }

                        if (user == null)
                        {
                            return NotFound();//This Google account is not associated to any user
                        }

                        tokenIssued = loginUserFromExternal(user, user.UserProfile.FirstName + " " + user.UserProfile.LastName);
                        tokenIssued.hasPassword = HasPassword;
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                    return Ok(tokenIssued);
                }
                else
                {
                    return BadRequest("Couldn't get Google information");
                }
            }

        }


        private bool createUserFromFacebook(FacebookRegisterModel facebookRegisterModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return false;
                }
                using (var context = new ApplicationDbContext())
                {
                    var newUserProfile = new UserProfile()
                    {
                        ApplicationUserId = facebookRegisterModel.IdentityUserId
                    };
                    
                    //if (facebookRegisterModel.Birthdate != null)
                    //{
                    //    newUserProfile.Birthdate = DateTime.Parse(facebookRegisterModel.Birthdate);
                    //}
                    
                    if (!string.IsNullOrEmpty(facebookRegisterModel.FirstName) && !string.IsNullOrEmpty(facebookRegisterModel.FirstName))
                    {
                        newUserProfile.FirstName = facebookRegisterModel.FirstName;
                        newUserProfile.LastName = facebookRegisterModel.LastName;
                    }
                    //newUserProfile.Gender = facebookRegisterModel.Gender;
                    newUserProfile.DateRegistered = DateTime.Now;
                    try
                    {
                        context.UserProfiles.Add(newUserProfile);
                    }
                    catch (Exception)
                    {
                        throw new Exception("Couldn't create user profile");
                    }

                    context.SaveChanges();
                    return true;
                }

            }
            catch (Exception)
            {
                return false;
            }

        }

        private LoginResponse loginUserFromExternal(ApplicationUser user, string names)
        {
            // Sign-in the user using the OWIN flow
            var identity = new ClaimsIdentity(Startup.OAuthOptions.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName, null, "Social"));
            // This is very important as it will be used to populate the current user id
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id, null, "LOCAL_AUTHORITY"));
            AuthenticationTicket ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
            var currentUtc = new Microsoft.Owin.Infrastructure.SystemClock().UtcNow;
            ticket.Properties.IssuedUtc = currentUtc;
            ticket.Properties.ExpiresUtc = currentUtc.Add(TimeSpan.Zero);
            var accesstoken = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket);
            UserProfile user_info;
            //Persist token in the DB
            using (var dbContext = new ApplicationDbContext())
            {
                dbContext.Tokens.Add(
               new Token
               {
                   Code = accesstoken,
                   ApplicationUserId = user.Id
               });
                user_info = user.UserProfile;
                user_info.DateLastLogin = DateTime.Now;
                dbContext.Entry(user_info).State = System.Data.Entity.EntityState.Modified;
                dbContext.SaveChanges();
            }

            return new LoginResponse()
            {
                access_token = accesstoken,
                token_type = "bearer",
                userName = user.UserName,
                email = user.Email,
                names = names,
                //birthday = user_info.Birthdate.ToString(),
                firstName = user_info.FirstName,
                lastName = user_info.LastName,
                //gender = user_info.Gender
            };
        }

        // POST api/Account/CheckEmail
        [AllowAnonymous]
        [Route("CheckEmail")]
        public async Task<IHttpActionResult> CheckEmail(CheckEmailModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await UserManager.FindByEmailAsync(model.Email);
            if (result == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        // POST api/Account/CheckUsername
        [AllowAnonymous]
        [Route("CheckUsername")]
        public async Task<IHttpActionResult> CheckUsername(CheckuserModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await UserManager.FindByNameAsync(model.Username);
            if (result == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }
        // PUT api/Account/UpdateProfile
        [HttpPut]
        [Route("UpdateProfile")]
        public async Task<IHttpActionResult> UpdateProfile(UpdateProfileBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = UserManager.FindByName(User.Identity.GetUserName());
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var userProfile = db.UserProfiles.First(x => x.ApplicationUserId == user.Id);
                    userProfile.FirstName = model.FirstName;
                    userProfile.LastName = model.LastName;
                    //userProfile.Birthdate = model.Birthdate;
                    //userProfile.Gender = model.Gender;

                    db.Entry(userProfile).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();
                    return Ok();
                }
            }
            catch (Exception)
            {
                return InternalServerError(new Exception("Couldn't update user's profile"));
            }

        }

        // POST api/Account/ForgotPassword
        [AllowAnonymous]
        [Route("ForgotPassword")]
        public async Task<IHttpActionResult> ForgotPassword([FromBody]ForgotPasswordModel emailModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await UserManager.FindByEmailAsync(emailModel.Email);
            if (user != null)
            {
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = new Uri(Url.Link("ResetLink", new { userId = user.Id, code = code }));
                string name = "";
                using (var dbContext = new ApplicationDbContext())
                {
                    var userProfile = dbContext.UserProfiles.FirstOrDefault(x => x.ApplicationUserId == user.Id);
                    name = userProfile.FirstName + " " + userProfile.LastName;
                }
                
                var emailRespone = await MailSender.SendForgotPassMail(emailModel.Email, name, user.UserName, callbackUrl.ToString());
                if (System.Net.HttpStatusCode.Accepted == emailRespone.StatusCode)
                {
                    return Ok("We've sent you a link to confirm your password reset");
                }
                
                return InternalServerError(new Exception("An error has occurred while sending the email"));
            }
            else
            {
                return BadRequest("User not found");
            }

        }

        // POST api/Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [Route("ResetPassword", Name = "ResetPassword")]
        public async Task<IHttpActionResult> ResetPassword(ResetPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await UserManager.FindByIdAsync(model.UserId);
            var result = await UserManager.ResetPasswordAsync(model.UserId, model.Code, model.NewPassword);
            if (result.Succeeded)
            {
                //Delete all tokens generated of this user
                using (var dbContext = new ApplicationDbContext())
                {
                    dbContext.Tokens.RemoveRange(dbContext.Tokens.Where(x => x.ApplicationUser.UserName.Equals(user.UserName, StringComparison.InvariantCultureIgnoreCase)));
                    dbContext.SaveChanges();
                }
                return Ok();
            }

            return BadRequest("Reset Password Form invalid");


        }

        // GET: /Account/ConfirmEmail
        [HttpGet]
        [AllowAnonymous]
        [Route("ConfirmEmail", Name = "ConfirmEmailRoute")]
        public async Task<IHttpActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return BadRequest(ModelState);
            }
            var resultComfirmed = await UserManager.IsEmailConfirmedAsync(userId);
            if (resultComfirmed)
            {
                return Ok("Your account has been confirmed.");
            }

            var result = await UserManager.ConfirmEmailAsync(userId, code);
            if (result.Succeeded)
            {
                return Ok("Account Confirmed.");
            }
            else
            {
                return BadRequest(ModelState);
            }

        }

        //
        // POST: /Account/ResendEmail
        [HttpPost]
        [AllowAnonymous]
        [Route("ResendEmail", Name = "ResendEmailRoute")]
        public async Task<IHttpActionResult> ResendEmail(CheckEmailModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user != null)
            {
                string name = "";
                using (var dbContext = new ApplicationDbContext())
                {
                    var userProfile = dbContext.UserProfiles.FirstOrDefault(x => x.ApplicationUserId == user.Id);
                    name = userProfile.FirstName + " " + userProfile.LastName;
                }
                string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);//await GetConfirmationToken(email);
                                                                                             //var callbackUrl = HttpUtility.UrlEncode(Request.RequestUri.GetLeftPart(UriPartial.Authority) + "/api/Account/ConfirmEmail?userId=" + user.Id + "&code=" + code);
                var callbackUrl = new Uri(Url.Link("ConfirmEmailRoute", new { userId = user.Id, code = code }));

                var emailRespone = await MailSender.SendAccountConfirmMail(model.Email, name, user.UserName, callbackUrl.ToString());
                if (System.Net.HttpStatusCode.Accepted == emailRespone.StatusCode)
                {
                    return Ok("We've sent you a link to confirm your account");
                }

                return InternalServerError(new Exception("An error has occurred while sending the email"));

            }
            else
            {
                return BadRequest("Email not found");
            }
        }

        // GET 
        //api/Account/GetProfile
        [HttpGet]
        [Route("GetProfile")]
        public async Task<IHttpActionResult> GetProfile()
        {
            var user = UserManager.FindByName(User.Identity.GetUserName());
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var userProfile = await db.UserProfiles.FirstAsync(x => x.ApplicationUserId == user.Id);
                   
                    return Ok(userProfile);
                }
            }
            catch (Exception)
            {
                return InternalServerError(new Exception("Couldn't get user's profile"));
            }

        }
        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
        
    }
}
