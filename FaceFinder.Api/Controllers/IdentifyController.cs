﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using FaceFinder.Data.DBContext;
using FaceFinder.Models.Entities;
using FaceFinder.Models.Api.Models;
using System.Web;
using FaceFinder.Data.Helpers;
using FaceFinder.Api.CustomCode;

namespace FaceFinder.Api.Controllers
{
    [CustomAuthorize]
    public class IdentifyController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // POST: api/Identify
        [ResponseType(typeof(IEnumerable<Person>))]
        public async Task<IHttpActionResult> PostIditify()
        {
            try
            {
                // Check if the request contains multipart/form-data.
                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }
                var currentReq = HttpContext.Current.Request;
                // Read the form data.
                var httpPostedFile = currentReq.Files["Media"];
                if (httpPostedFile == null)
                {
                    return BadRequest("No file provided");
                }
                //Get the file

                int MaxContentLength = 1024 * 1024 * 4; //Size = 4 MB

                //Check file extension
                IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                var ext = httpPostedFile.FileName.Substring(httpPostedFile.FileName.LastIndexOf('.'));
                var extension = ext.ToLower();
                if (!AllowedFileExtensions.Contains(extension))
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }
                var byteArray = StorageHelper.ConverToBytes(new HttpPostedFileWrapper(httpPostedFile));
                //Check file size, if is more than 4 MB, reduce it until get it
                if (byteArray.Length > MaxContentLength)
                {
                    byteArray = StorageHelper.ResizeImageFileSize(byteArray, MaxContentLength);
                }
                var identifiedPersons = await FaceRecognitionHelper.IdentifyUsersFacesInPhoto(byteArray);
                var currentUserIdentity = (CustomGenericIdentity)User.Identity;
                var autoAlertValue = currentReq.Form["AutoAlert"];
                var autoAlert = false;
                if (autoAlertValue != null)
                {
                    autoAlert = bool.Parse(autoAlertValue);
                }

                double? lat = null;
                double? lon = null;
                var latValue = currentReq.Form["Latitude"];
                var lonValue = currentReq.Form["Longitude"];
                if (lonValue != null && latValue != null)
                {
                    lat = double.Parse(latValue);
                    lon = double.Parse(lonValue);
                }
                if (autoAlertValue != null)
                {
                    autoAlert = bool.Parse(autoAlertValue);
                }
                if (autoAlert)
                {
                    foreach (var item in identifiedPersons)
                    {
                        db.Alerts.Add(new Alert
                        {
                            DateCreated = DateTime.Now,
                            Latitude = lat,
                            Longitude = lon,
                            PersonId = item.Id,
                            UserProfileApplicationUserId = currentUserIdentity.Id
                        });
                        //TODO: Send alerts though email
                    }

                    await db.SaveChangesAsync();
                }

                return Ok(identifiedPersons);
            }
            catch (Exception ex)
            {
                throw ex;
            }
           

        }

        // POST: api/Search
        [HttpPost]
        [Route("api/Search")]
        [ResponseType(typeof(IEnumerable<Person>))]
        public IHttpActionResult Search(SearchByNameModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var text = model.Text.ToLower();
            var result = db.Persons.Include(p => p.FaceImages)
                .Where(p => !p.Disabled && 
                (p.FirstName.ToLower().Contains(text)
                || p.LastName.ToLower().Contains(text)
                || p.NickName.ToLower().Contains(text)));

            return Ok(result);
        }

        // POST: api/Alert
        [HttpPost]
        [Route("api/Alert")]
        [ResponseType(typeof(IEnumerable<Person>))]
        public IHttpActionResult Alert(AddAlertModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var currentUserIdentity = (CustomGenericIdentity)User.Identity;
            db.Alerts.Add(new Alert
            {
                DateCreated = DateTime.Now,
                Latitude = model.Latitude,
                Longitude = model.Longitude,
                PersonId = model.PersonId,
                UserProfileApplicationUserId = currentUserIdentity.Id
            });
            db.SaveChanges();
            return Ok();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}