﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace FaceFinder.Api.CustomCode
{
    public class CustomGenericIdentity : GenericIdentity
    {
        public string Id { get; set; }
        public CustomGenericIdentity(string userName, string userId) : base(userName)
        {
            Id = userId;
        }
    }
}