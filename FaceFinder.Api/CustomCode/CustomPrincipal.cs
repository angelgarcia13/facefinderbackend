﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace FaceFinder.Api.CustomCode
{
    public class CustomPrincipal : IPrincipal
    {
        public IIdentity Identity { get; private set; }
        public CustomPrincipal(string userName, string Id)
        {
            this.Id = Id;
            this.Identity = new GenericIdentity(userName);
        }
        public bool IsInRole(string role)
        {
            return true;
        }
        public string Id { get; set; }
    }
}