﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;

namespace FaceFinder.Api.CustomCode
{
    public static class MailSender
    {
        public static async Task<dynamic> SendForgotPassMail(string toEmail, string name, string userName, string callbackUrl)
        {
            string apiKey = WebConfigurationManager.AppSettings["SENDGRID_KEY"].ToString();
            //dynamic sg = new SendGridAPIClient(apiKey, "https://api.sendgrid.com");

            dynamic sg = new SendGridAPIClient(apiKey);

            Email from = new Email(WebConfigurationManager.AppSettings["SENDGRID_FROM"].ToString(), "FaceFinder Support");
            String subject = "Reset your FaceFinder password";
            Email to = new Email(toEmail);
            Content content = new Content("text/html", "<br/>");
            Mail mail = new Mail(from, subject, to, content);
            mail.TemplateId = WebConfigurationManager.AppSettings["SENDGRID_TEMPLATE_FORGOTPASS"].ToString();
            mail.Personalization[0].AddSubstitution("-name-", name);
            mail.Personalization[0].AddSubstitution("-username-", userName);
            mail.Personalization[0].AddSubstitution("-apiresetlink-", callbackUrl);


            dynamic response = await sg.client.mail.send.post(requestBody: mail.Get());
            return response;
        }

        public static async Task<dynamic> SendAccountConfirmMail(string toEmail, string name, string userName, string callbackUrl)
        {
            string apiKey = WebConfigurationManager.AppSettings["SENDGRID_KEY"].ToString();
            //dynamic sg = new SendGridAPIClient(apiKey, "https://api.sendgrid.com");

            dynamic sg = new SendGridAPIClient(apiKey);

            Email from = new Email(WebConfigurationManager.AppSettings["SENDGRID_FROM"].ToString(), "FaceFinder Support");
            String subject = "Confirm your FaceFinder account";
            Email to = new Email(toEmail);
            Content content = new Content("text/html", "<br/>");
            Mail mail = new Mail(from, subject, to, content);
            mail.TemplateId = WebConfigurationManager.AppSettings["SENDGRID_TEMPLATE_CONFIRM"].ToString();
            mail.Personalization[0].AddSubstitution("-name-", name);
            mail.Personalization[0].AddSubstitution("-username-", userName);
            mail.Personalization[0].AddSubstitution("-apiresetlink-", callbackUrl);


            dynamic response = await sg.client.mail.send.post(requestBody: mail.Get());
            return response;
        }


        public static async Task<dynamic> SendMail(string toEmail, string message)
        {
            string apiKey = WebConfigurationManager.AppSettings["SENDGRID_KEY"].ToString();
            //dynamic sg = new SendGridAPIClient(apiKey, "https://api.sendgrid.com");

            dynamic sg = new SendGridAPIClient(apiKey);

            Email from = new Email(WebConfigurationManager.AppSettings["SENDGRID_FROM"].ToString());
            String subject = "Reset your FaceFinder password";
            Email to = new Email(toEmail);
            Content content = new Content("text/html", "<br/> " + message);
            Mail mail = new Mail(from, subject, to, content);

            dynamic response = await sg.client.mail.send.post(requestBody: mail.Get());
            return response;
        }
    }
}