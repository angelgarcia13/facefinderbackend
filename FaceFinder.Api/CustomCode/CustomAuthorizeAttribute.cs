﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using FaceFinder.Data.DBContext;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace FaceFinder.Api.CustomCode
{
    public class CustomAuthorizeAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext filterContext)
        {

            if (SkipAuthorization(filterContext))
            {
                return;
            }

            bool isValidToken = false;
            if (filterContext.Request.Headers.Authorization != null)
            {
                if (filterContext.Request.Headers.Authorization.Scheme.Equals("bearer", StringComparison.InvariantCultureIgnoreCase))
                {

                    using (var dbContext = new ApplicationDbContext())
                    {
                        var tokenExists = dbContext.Tokens.Include("ApplicationUser.UserProfile").FirstOrDefault(x => x.Code == filterContext.Request.Headers.Authorization.Parameter && !x.ApplicationUser.UserProfile.Disabled);
                        if (tokenExists != null)
                        {
                            var userManager = filterContext.Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
                            //ApplicationUser user = userManager.FindByName(tokenExists.ApplicationUser.UserName);
                            var roles = userManager.GetRoles(tokenExists.ApplicationUser.Id);
                            //IPrincipal principal = new CustomPrincipal(tokenExists.ApplicationUser.UserName, tokenExists.ApplicationUserId);
                            IPrincipal principal = new GenericPrincipal(
                                   new CustomGenericIdentity(tokenExists.ApplicationUser.UserName, tokenExists.ApplicationUser.Id),
                                   roles.Select(x => x).ToArray()
                               );
                            HttpContext.Current.User = principal;

                            isValidToken = true;
                        }


                    }
                }
                if (!isValidToken)
                {
                    filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                    {
                        ReasonPhrase = "Invalid Token"
                    };
                }
            }
            else
            {
                filterContext.Response = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Invalid Request"
                };
            }

            base.OnAuthorization(filterContext);

        }

        private static bool SkipAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            Contract.Assert(actionContext != null);

            return actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any()
                   || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
        }
    }
}