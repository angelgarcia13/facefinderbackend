﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using FaceFinder.Models.Entities;
using FaceFinder.Data.DBContext;
using System.Linq;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace FaceFinder.Api.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;
        private ApplicationUser user;
        public ApplicationOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

            user = await userManager.FindAsync(context.UserName, context.Password);
            UserProfile user_info;
            
            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }
            
            using (var contextDB = new ApplicationDbContext())
            {
                
                user_info = contextDB.UserProfiles.Where(q => q.ApplicationUserId == user.Id && !q.Disabled).FirstOrDefault();

                if (user_info == null)
                {
                    context.SetError("invalid_grant", "This user doesn't have a profile created.");
                    return;
                }
                user_info.DateLastLogin = DateTime.Now;
                contextDB.Entry(user_info).State = System.Data.Entity.EntityState.Modified;
                contextDB.SaveChanges();
                


            }
            
            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
               OAuthDefaults.AuthenticationType);
            oAuthIdentity.AddClaim(new Claim("IdentityId", user.Id));
            ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager,
                CookieAuthenticationDefaults.AuthenticationType);
            cookiesIdentity.AddClaim(new Claim("IdentityId", user.Id));
            AuthenticationProperties properties = CreateProperties(user.UserName, (user_info.FirstName + " " + user_info.LastName) ?? "", user.Id, true, user_info.FirstName ?? "", user_info.LastName ?? ""/*, user_info.Birthdate, user_info.Gender ?? ""*/);
            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated(ticket);
            context.Request.Context.Authentication.SignIn(cookiesIdentity);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                //removed .issued and .expires parameter
                if (!property.Key.StartsWith("."))
                {
                    context.AdditionalResponseParameters.Add(property.Key, property.Value);

                }
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName, string names, string IdentityId, bool hasPassword, string firstName, string lastName/*, DateTime? birthday, string gender*/)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName },
                { "names", names },
                { "identityId", IdentityId},
                { "hasPassword", hasPassword.ToString()},
                { "firstName", firstName},
                { "lastName", lastName}/*,
                { "birthday", birthday.ToString()},
                { "gender", gender ?? string.Empty}*/
            };

            return new AuthenticationProperties(data);
        }
        public override Task TokenEndpointResponse(OAuthTokenEndpointResponseContext context)
        {
            //Save the AccessToken from context to the DB, after that, the AuthFilter will be ready to read the token from the DB.
            //context.AccessToken
            var tokenGenerated = context.AccessToken;

            var userName = context.TokenEndpointRequest.Parameters.ToArray().Single(x => x.Key == "UserName").Value[0];
            using (var dbContext = new ApplicationDbContext())
            {
                //var existingEntry = dbContext.Tokens.FirstOrDefault(x => x.User.Equals(userName, StringComparison.InvariantCultureIgnoreCase));
                //var userToken = existingEntry.Token1;
                try
                {
                    dbContext.Tokens.Add(
                        new Token
                        {
                            Code = tokenGenerated,
                            ApplicationUserId = user.Id
                        });

                    dbContext.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}",
                                                    validationError.PropertyName,
                                                    validationError.ErrorMessage);
                        }
                    }
                    throw dbEx;
                }



            }

            return base.TokenEndpointResponse(context);
        }
    }
}