﻿$(document).ready(function () {
    $("#resetButton").click(function () {
        var button = $(this);
        button.prop('disabled', true);
        var data = {
            UserId: $("#userId").val(),
            Code: $("#code").val(),
            NewPassword: $("#password").val(),
            ConfirmPassword: $("#repeatpassword").val()
        };
        $("#loadingDiv").show();
        $("#formDiv").hide();
        $.ajax({
            type: "POST",
            url: "api/Account/ResetPassword",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            //dataType: "json",
            success: function (msg) {
                // Do something interesting here.
                $("#message").html("Your password has been reset!");
                $("#loadingDiv").hide();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                if (XMLHttpRequest.status == 400) {
                    $('#modal1').find("p").html(XMLHttpRequest.responseJSON.Message);
                } else {
                    $('#modal1').find("p").html("An error has occurred, please try again. If the error continues contact the administrator.");
                }
                button.prop('disabled', false);
                $("#loadingDiv").hide();
                $("#formDiv").show();
                $("#modal1").openModal("open");
            }
        });
    });
});