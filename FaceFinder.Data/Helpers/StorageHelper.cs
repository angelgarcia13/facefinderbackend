﻿using ImageProcessor;
using ImageProcessor.Imaging.Formats;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FaceFinder.Data.Helpers
{
    public static class StorageHelper
    {

        // Parse the connection string and return a reference to the storage account.
        static CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            CloudConfigurationManager.GetSetting("StorageConnectionString"));

        // Create the blob client.
        static CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

        public static async Task InitContainers()
        {
            // Retrieve a reference to a container.
            CloudBlobContainer container = blobClient.GetContainerReference("faces");
            // Create the container if it doesn't already exist.
            container.CreateIfNotExists();
            //Set public access to the container
            await container.SetPermissionsAsync(
                new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

            // Retrieve a reference to a container.
            CloudBlobContainer containerUsers = blobClient.GetContainerReference("users");
            // Create the container if it doesn't already exist.
            await containerUsers.CreateIfNotExistsAsync();
            //Set public access to the container
            await containerUsers.SetPermissionsAsync(
               new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

        }

        public static async Task UploadImage(string userId, int postId, string extension, byte[] fileStream)
        {
            try
            {
                // Retrieve reference to a previously created container.
                CloudBlobContainer container = blobClient.GetContainerReference("faces");

                // Retrieve reference to a blob.
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(userId + "/" + postId + extension);

                // Create or overwrite the blob with contents from a file.
                blockBlob.UploadFromByteArray(fileStream, 0, fileStream.Length);

                // Retrieve reference to a blob (thumb).
                CloudBlockBlob blockThumbBlob = container.GetBlockBlobReference(userId + "/" + postId + "-small" + extension);

                // Create or overwrite the blob (thumb) with contents from a file.
                var redimImage = fileStream.ResizeImage();
                await blockThumbBlob.UploadFromByteArrayAsync(redimImage, 0, redimImage.Length);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static async Task UploadUserPhoto(string userId, byte[] fileStream)
        {
            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("users");

            // Retrieve reference to a blob.
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(userId + ".jpg");

            // Create or overwrite the blob with contents from a file.
            await blockBlob.UploadFromByteArrayAsync(fileStream, 0, fileStream.Length);

        }

        public static async Task DeleteImage(string userId, int postId, string extension)
        {
            // Retrieve reference to a created container.
            CloudBlobContainer container = blobClient.GetContainerReference("faces");

            // Retrieve reference to a blob.
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(userId + "/" + postId + extension);

            // Delete the blob.
            await blockBlob.DeleteIfExistsAsync();

            // Retrieve reference to a blob (thumb).
            CloudBlockBlob blockThumbBlob = container.GetBlockBlobReference(userId + "/" + postId + "-small" + extension);

            // Delete the blob (thumb).
            await blockThumbBlob.DeleteIfExistsAsync();
        }
        public static byte[] ConverToBytes(HttpPostedFileBase Image)
        {
            byte[] imgBytes = null;
            using (BinaryReader reader = new BinaryReader(Image.InputStream))
            {
                imgBytes = reader.ReadBytes(Image.ContentLength);
                Image.InputStream.Position = 0;
                return imgBytes;
            }
        }

        public static byte[] ResizeImage(this byte[] photoBytes)
        {
            // Format is automatically detected though can be changed.

            ISupportedImageFormat format = new JpegFormat { Quality = 100 };
            Size size = new Size(480, 0);
            using (MemoryStream inStream = new MemoryStream(photoBytes))
            {
                using (MemoryStream outStream = new MemoryStream())
                {
                    // Initialize the ImageFactory using the overload to preserve EXIF metadata.
                    using (ImageFactory imageFactory = new ImageFactory(preserveExifData: true))
                    {
                        // Load, resize, set the format and quality and save an image.
                        imageFactory.Load(inStream)
                                    .Resize(size)
                                    .Format(format)
                                    .Save(outStream);
                    }
                    // Do something with the stream.
                    return outStream.ToArray();
                }
            }
        }
        public static byte[] ResizeImageSize(byte[] photoBytes, int reductionPercent)
        {
            
            using (MemoryStream inStream = new MemoryStream(photoBytes))
            {
                // Initialize the ImageFactory using the overload to preserve EXIF metadata.
                using (ImageFactory imageFactory = new ImageFactory(preserveExifData: true))
                {
                    imageFactory.Load(inStream);
                    var actualWidth = imageFactory.Image.Width;
                    var newWith = (actualWidth * (100 - reductionPercent)) / 100;
                    Size size = new Size(newWith, 0);
                    using (MemoryStream outStream = new MemoryStream())
                    {
                        // Load, resize, set the format and quality and save an image.
                        imageFactory.Resize(size)
                                    .Save(outStream);
                        // Do something with the stream.
                        return outStream.ToArray();
                    }
                }
            }
        }
        public static byte[] ResizeImageFileSize(byte[] photoBytes, int MaxSize)
        {
            var actualSize = photoBytes.Length;
            while (actualSize > MaxSize)
            {
                photoBytes = ResizeImageSize(photoBytes, 20);
                actualSize = photoBytes.Length;
            }
            return photoBytes;
        }
        public static string ContainerUrl = ConfigurationManager.AppSettings["BlobStorageHost"].ToString();
    }
}
