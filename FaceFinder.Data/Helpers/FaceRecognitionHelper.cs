﻿using FaceFinder.Data.DBContext;
using FaceFinder.Models.Api.Models;
using Microsoft.ProjectOxford.Face;
using Microsoft.ProjectOxford.Face.Contract;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace FaceFinder.Data.Helpers
{
    public static class FaceRecognitionHelper
    {
        static FaceServiceClient faceServiceClient = new FaceServiceClient(WebConfigurationManager.AppSettings["FaceAPIKey"].ToString(), "https://westeurope.api.cognitive.microsoft.com/face/v1.0");
        public const string groupHost = "facefinder-";
        public static async Task<PersonCreatedModel> CreatePerson(string name, int id)
        {
            Models.Entities.PersonGroup activePersonGroup;
            using (var db = new ApplicationDbContext())
            {
                activePersonGroup = db.PersonGroups.FirstOrDefault(x => !x.Completed);
                try
                {
                    if (activePersonGroup == null)
                    {
                        activePersonGroup = await CreateNewPersonGroup();
                    }

                    var result = await faceServiceClient.CreatePersonAsync(activePersonGroup.GroupGuid, name, id.ToString());
                    await TrainPersonGroup(activePersonGroup.GroupGuid);
                    return new PersonCreatedModel
                    {
                        PersonId = result.PersonId,
                        Success = true,
                        PersonGroupId = activePersonGroup.Id
                    };
                }
                catch (FaceAPIException ex)
                {
                    if (ex.ErrorCode == "PersonGroupTrainingNotFinished")
                    {
                        return new PersonCreatedModel
                        {
                            PersonId = new Guid(),
                            Success = false
                        };
                    }
                    if (ex.ErrorCode == "QuotaExceeded")
                    {
                        activePersonGroup.Completed = true;
                        db.Entry(activePersonGroup).State = System.Data.Entity.EntityState.Modified;
                        await db.SaveChangesAsync();
                        return await CreatePerson(name, id);
                    }
                    throw ex;
                }
            }
        }

        public static async Task DeletePerson(string id, string groupId, bool fromJob = false)
        {
            try
            {
                await faceServiceClient.DeletePersonAsync(groupId, new Guid(id));
                await TrainPersonGroup(groupId);
            }
            catch (FaceAPIException ex)
            {
                //Add to the Deletion Table
                if (!fromJob)
                {
                    //TODO: add Job to the failed attempts
                    /*
                    using (var db = new ApplicationDbContext())
                    {
                        var item = new ItemToDelete
                        {
                            ItemType = ItemToDeleteTypes.Person,
                            GroupName = groupId,
                            PersonId = new Guid(id)
                        };
                        await db.SaveChangesAsync();
                    }
                    */
                }
                else
                {
                    throw ex;
                }
            }
        }

        private static async Task<Models.Entities.PersonGroup> CreateNewPersonGroup()
        {
            var codeGenerated = GenerateCode();
            var newCodeName = $"{groupHost}{codeGenerated.ToLower()}";
            try
            {

                await faceServiceClient.CreatePersonGroupAsync(newCodeName, newCodeName);

                using (var db = new ApplicationDbContext())
                {
                    var newGroup = new FaceFinder.Models.Entities.PersonGroup
                    {
                        GroupGuid = newCodeName
                    };
                    db.PersonGroups.Add(newGroup);
                    await db.SaveChangesAsync();
                    return newGroup;
                }
            }
            catch (FaceAPIException ex)
            {
                if (ex.ErrorCode == "PersonGroupExists")
                {
                    return await CreateNewPersonGroup();
                }
                throw ex;
            }
        }

        public static async Task TrainPersonGroup(string personGroupId)
        {
            TrainingStatus trainingStatus = null;
            //Verify the training is not running
            try
            {
                while (true)
                {
                    trainingStatus = await faceServiceClient.GetPersonGroupTrainingStatusAsync(personGroupId);

                    if (trainingStatus.Status != Status.Running)
                    {
                        break;
                    }

                    await Task.Delay(1000);
                }
                await faceServiceClient.TrainPersonGroupAsync(personGroupId);

            }
            catch (FaceAPIException ex)
            {
                //"PersonGroupNotTrained"
                if (ex.ErrorCode == "PersonGroupNotTrained")
                {
                    try
                    {
                        await faceServiceClient.TrainPersonGroupAsync(personGroupId);
                    }
                    catch (FaceAPIException ex2)
                    {

                        throw ex2;
                    }
                }
                else if (ex.ErrorCode == "PersonGroupTrainingNotFinished")
                {
                    //Nothing to do
                    throw ex;
                }
                else
                {
                    //Logg
                    System.Diagnostics.Trace.TraceError("Error trying to train person group (FaceApi): " + ex.ErrorMessage);
                    throw ex;
                }
            }

        }

        public static async Task DeletePersonFace(Guid personFaceId, Guid personId, string groupId)
        {
            try
            {
                await faceServiceClient.DeletePersonFaceAsync(groupId, personId, personFaceId);
                await TrainPersonGroup(groupId);

            }
            catch (FaceAPIException ex)
            {
                //Loggin
                throw ex;
            }
        }
        public static async Task<string> AddPersonFace(Guid personId, string ApplicationUserId, string groupId, byte[] image)
        {
            try
            {
                using (Stream stream = new MemoryStream(image))
                {
                    //add new Face
                    var faceAdded = await faceServiceClient.AddPersonFaceAsync(groupId, personId, stream);
                    await TrainPersonGroup(groupId);
                    return faceAdded.PersistedFaceId.ToString();
                }

            }
            catch (FaceAPIException ex)
            {
                /*
                if (ex.ErrorCode == "PersonGroupTrainingNotFinished")
                {
                    throw ex;

                }
                */
                throw ex;

            }
        }

        public static async Task<IEnumerable<Models.Entities.Person>> IdentifyUsersFacesInPhoto(byte[] image)
        {
            Face[] facesDetected;
            var result = new List<Models.Entities.Person>();
            using (Stream stream = new MemoryStream(image))
            {
                facesDetected = await faceServiceClient.DetectAsync(stream, true, false);
            }
            if (facesDetected.Count() > 0)
            {
                List<Models.Entities.PersonGroup> groups = new List<Models.Entities.PersonGroup>();
                //Get the person groups from db
                using (var db = new ApplicationDbContext())
                {
                    groups = db.PersonGroups.ToList();

                    var facesIdArray = facesDetected.Select(f => f.FaceId).ToArray();
                    foreach (var item in groups)
                    {
                        IdentifyResult[] foundFaces;
                        try
                        {
                            foundFaces = await faceServiceClient.IdentifyAsync(item.GroupGuid, facesIdArray, 5);

                        }
                        catch (FaceAPIException ex)
                        {
                            if (ex.ErrorCode.Equals("PersonGroupNotTrained"))
                            {
                                await faceServiceClient.TrainPersonGroupAsync(item.GroupGuid);
                                foundFaces = await faceServiceClient.IdentifyAsync(item.GroupGuid, facesIdArray, 5);

                            }
                            else {
                                throw ex;
                            }
                            
                        }
                        foreach (var identifyItem in foundFaces)
                        {
                            //Get those persons from db
                            var personsIds = identifyItem.Candidates.Select(c => c.PersonId.ToString()).Distinct();
                            var personsFromDB = db.Persons
                                .Include(p => p.FaceImages)
                                .Where(p => !p.Disabled && personsIds.Contains(p.FacePersonId) && !result.Select(r => r.Id).Contains(p.Id)).ToList();
                            if (personsFromDB.Count > 0)
                            {
                                result.AddRange(personsFromDB);
                            }

                        }
                    }
                }
            }
            return result;
        }
        private static string GenerateCode()
        {
            Guid g = Guid.NewGuid();
            string GuidString = Convert.ToBase64String(g.ToByteArray());
            Random rnd = new Random();
            string rndNumber = rnd.Next(1, 9).ToString();
            GuidString = GuidString.Replace("=", rndNumber);
            rndNumber = rnd.Next(1, 9).ToString();
            GuidString = GuidString.Replace("+", rndNumber);
            rndNumber = rnd.Next(1, 9).ToString();
            GuidString = GuidString.Replace("/", rndNumber);
            rndNumber = rnd.Next(1, 9).ToString();
            GuidString = GuidString.Replace(".", rndNumber);
            GuidString = GuidString.Substring(0, 7).ToUpperInvariant();
            using (var db = new ApplicationDbContext())
            {
                if (!db.PersonGroups.Any(x => x.GroupGuid.Equals(groupHost + GuidString, StringComparison.InvariantCultureIgnoreCase)))
                {
                    return GuidString;
                }
                else
                {
                    return GenerateCode();
                }
            }

        }
    }
}
