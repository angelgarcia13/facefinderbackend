namespace FaceFinder.Data.Migrations.ApplicationDb
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models.Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<FaceFinder.Data.DBContext.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations\ApplicationDb";
        }

        protected override void Seed(FaceFinder.Data.DBContext.ApplicationDbContext context)
        {
            //UNCOMMENT THIS CONDITION TO DEBUG
            /*
            if (System.Diagnostics.Debugger.IsAttached == false)
                System.Diagnostics.Debugger.Launch();
            */

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //Adding Initial roles
            var initialRoles =
                new[] { "Agents", "Administrators", "Monitors"};
            foreach (var role in initialRoles)
            {
                if (!context.Roles.Any(r => r.Name == role))
                {
                    var roleStore = new RoleStore<IdentityRole>(context);
                    var roleManager = new RoleManager<IdentityRole>(roleStore);
                    roleManager.Create(new IdentityRole(role));
                }
            }

            //Adding default Admin User
            if (!(context.Users.Any(u => u.UserName == "admin@facefinder.com")))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var userToInsert = new ApplicationUser
                { UserName = "admin@facefinder.com", PhoneNumber = "0000000000", Email = "admin@facefinder.com" };
                userManager.Create(userToInsert, "Admin@123");
                userManager.AddToRole(userToInsert.Id, "Administrators");
            }
            //Adding default Admin User
            if (!(context.Users.Any(u => u.UserName == "angel@facefinder.com")))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var userToInsert = new ApplicationUser
                { UserName = "angel@facefinder.com", PhoneNumber = "0000000000", Email = "angel@facefinder.com" };
                userManager.Create(userToInsert, "Angel@123");
                userManager.AddToRole(userToInsert.Id, "Agents");
            }
            //Adding default Monitor User
            if (!(context.Users.Any(u => u.UserName == "estacion@facefinder.com")))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var userToInsert = new ApplicationUser
                { UserName = "estacion@facefinder.com", PhoneNumber = "0000000000", Email = "estacion@facefinder.com" };
                userManager.Create(userToInsert, "Estacion@123");
                userManager.AddToRole(userToInsert.Id, "Monitors");
            }
        }
    }
}
