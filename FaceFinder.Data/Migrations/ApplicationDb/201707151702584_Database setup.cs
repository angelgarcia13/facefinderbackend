namespace FaceFinder.Data.Migrations.ApplicationDb
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Databasesetup : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Facefinder.FaceImages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Extension = c.String(),
                        ContainerUrl = c.String(),
                        PersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Facefinder.People", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.PersonId);
            
            CreateTable(
                "Facefinder.People",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        NickName = c.String(),
                        WantedReasons = c.String(),
                        Notes = c.String(),
                        Birthdate = c.DateTime(),
                        Gender = c.String(),
                        DateRegistered = c.DateTime(),
                        DateLastLogin = c.DateTime(),
                        Disabled = c.Boolean(nullable: false),
                        FacePersonId = c.String(),
                        HasFacePersonId = c.Boolean(nullable: false),
                        PersonGroupId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Facefinder.PersonGroups", t => t.PersonGroupId)
                .Index(t => t.PersonGroupId);
            
            CreateTable(
                "Facefinder.PersonGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GroupGuid = c.String(),
                        Completed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Facefinder.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "Facefinder.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("Facefinder.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("Facefinder.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "Facefinder.Tokens",
                c => new
                    {
                        TokenId = c.Int(nullable: false, identity: true),
                        ApplicationUserId = c.String(nullable: false, maxLength: 128),
                        Code = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.TokenId)
                .ForeignKey("Facefinder.AspNetUsers", t => t.ApplicationUserId, cascadeDelete: true)
                .Index(t => t.ApplicationUserId);
            
            CreateTable(
                "Facefinder.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "Facefinder.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Facefinder.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "Facefinder.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("Facefinder.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "Facefinder.UserProfiles",
                c => new
                    {
                        ApplicationUserId = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                        DateRegistered = c.DateTime(),
                        DateLastLogin = c.DateTime(),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ApplicationUserId)
                .ForeignKey("Facefinder.AspNetUsers", t => t.ApplicationUserId)
                .Index(t => t.ApplicationUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Facefinder.Tokens", "ApplicationUserId", "Facefinder.AspNetUsers");
            DropForeignKey("Facefinder.UserProfiles", "ApplicationUserId", "Facefinder.AspNetUsers");
            DropForeignKey("Facefinder.AspNetUserRoles", "UserId", "Facefinder.AspNetUsers");
            DropForeignKey("Facefinder.AspNetUserLogins", "UserId", "Facefinder.AspNetUsers");
            DropForeignKey("Facefinder.AspNetUserClaims", "UserId", "Facefinder.AspNetUsers");
            DropForeignKey("Facefinder.AspNetUserRoles", "RoleId", "Facefinder.AspNetRoles");
            DropForeignKey("Facefinder.FaceImages", "PersonId", "Facefinder.People");
            DropForeignKey("Facefinder.People", "PersonGroupId", "Facefinder.PersonGroups");
            DropIndex("Facefinder.UserProfiles", new[] { "ApplicationUserId" });
            DropIndex("Facefinder.AspNetUserLogins", new[] { "UserId" });
            DropIndex("Facefinder.AspNetUserClaims", new[] { "UserId" });
            DropIndex("Facefinder.AspNetUsers", "UserNameIndex");
            DropIndex("Facefinder.Tokens", new[] { "ApplicationUserId" });
            DropIndex("Facefinder.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("Facefinder.AspNetUserRoles", new[] { "UserId" });
            DropIndex("Facefinder.AspNetRoles", "RoleNameIndex");
            DropIndex("Facefinder.People", new[] { "PersonGroupId" });
            DropIndex("Facefinder.FaceImages", new[] { "PersonId" });
            DropTable("Facefinder.UserProfiles");
            DropTable("Facefinder.AspNetUserLogins");
            DropTable("Facefinder.AspNetUserClaims");
            DropTable("Facefinder.AspNetUsers");
            DropTable("Facefinder.Tokens");
            DropTable("Facefinder.AspNetUserRoles");
            DropTable("Facefinder.AspNetRoles");
            DropTable("Facefinder.PersonGroups");
            DropTable("Facefinder.People");
            DropTable("Facefinder.FaceImages");
        }
    }
}
