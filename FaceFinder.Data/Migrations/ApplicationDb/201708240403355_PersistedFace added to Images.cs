namespace FaceFinder.Data.Migrations.ApplicationDb
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PersistedFaceaddedtoImages : DbMigration
    {
        public override void Up()
        {
            AddColumn("Facefinder.FaceImages", "PersistedFaceId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("Facefinder.FaceImages", "PersistedFaceId");
        }
    }
}
