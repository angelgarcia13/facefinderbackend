namespace FaceFinder.Data.Migrations.ApplicationDb
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Alertsmodel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Facefinder.Alerts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateCreated = c.DateTime(nullable: false),
                        Longitude = c.Double(),
                        Latitude = c.Double(),
                        UserProfileApplicationUserId = c.String(maxLength: 128),
                        PersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Facefinder.People", t => t.PersonId, cascadeDelete: true)
                .ForeignKey("Facefinder.UserProfiles", t => t.UserProfileApplicationUserId)
                .Index(t => t.UserProfileApplicationUserId)
                .Index(t => t.PersonId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Facefinder.Alerts", "UserProfileApplicationUserId", "Facefinder.UserProfiles");
            DropForeignKey("Facefinder.Alerts", "PersonId", "Facefinder.People");
            DropIndex("Facefinder.Alerts", new[] { "PersonId" });
            DropIndex("Facefinder.Alerts", new[] { "UserProfileApplicationUserId" });
            DropTable("Facefinder.Alerts");
        }
    }
}
