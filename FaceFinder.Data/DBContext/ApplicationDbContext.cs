﻿using Microsoft.AspNet.Identity.EntityFramework;
using FaceFinder.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceFinder.Data.DBContext
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            this.Configuration.ProxyCreationEnabled = false;
            //this.Configuration.LazyLoadingEnabled = false;
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("Facefinder");

            // Configure ID as PK for UserProfile
            modelBuilder.Entity<UserProfile>()
            .HasKey(s => s.ApplicationUserId);
            modelBuilder.Entity<UserProfile>()
            .HasRequired(t => t.ApplicationUser);

            modelBuilder.Entity<UserProfile>()
            .Ignore(t => t.Name);
            // Configure ID as FK for UserProfile
            //modelBuilder.Entity<ApplicationUser>()
            //.HasRequired(t => t.UserProfile)
            //.WithRequiredPrincipal(t => t.ApplicationUser);

            // Configure Code as Required for Token
            modelBuilder.Entity<Token>()
            .Property(t => t.Code).IsRequired();

            //one-to-many 
            modelBuilder.Entity<Token>()
                        .HasRequired(t => t.ApplicationUser) // Token entity requires ApplicationUser
                        .WithMany(t => t.Tokens); // ApplicationUser entity includes many Token entities

            //one-to-many 
            modelBuilder.Entity<FaceImage>()
                        .HasRequired(t => t.Person)
                        .WithMany(t => t.FaceImages);

            //Ignoring ImageUrl prop
            modelBuilder.Entity<FaceImage>()
                .Ignore(t => t.ImageUrl);

            //Ignoring somre props
            modelBuilder.Entity<Person>()
                .Ignore(t => t.HasAge);

            modelBuilder.Entity<Person>()
                .Ignore(t => t.Age);

            modelBuilder.Entity<Person>()
                .Ignore(t => t.FormattedNickName);

            modelBuilder.Entity<Person>()
                .Ignore(t => t.Image);

            modelBuilder.Entity<Person>()
                .Ignore(t => t.HasImage);

            modelBuilder.Entity<Person>()
                .Ignore(t => t.HasNoImage);

            modelBuilder.Entity<Person>()
                .Ignore(t => t.Name);
        }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<Token> Tokens { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<PersonGroup> PersonGroups { get; set; }
        public virtual DbSet<FaceImage> FaceImages { get; set; }

        public virtual DbSet<Alert> Alerts { get; set; }
    }
}
